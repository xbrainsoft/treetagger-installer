if [ ! -d ~/treetagger ]
then
	mkdir -p ~/treetagger
else
	echo "treetagger already exists, skipping installation"
	exit
fi    

cd ~/treetagger
echo "downloading resources"
wget http://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/tree-tagger-linux-3.2.1.tar.gz
wget http://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/tagger-scripts.tar.gz
wget http://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/install-tagger.sh
wget http://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/english-par-linux-3.2-utf8.bin.gz
wget http://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/french-par-linux-3.2-utf8.bin.gz
echo "installing treetagger"
sh install-tagger.sh
echo 'hello world!' | cmd/tree-tagger-english 
echo 'Bonjour tout le monde !' | cmd/tree-tagger-french 
echo "adding treetager to path"
export PATH=$PATH:/home/xbrain/treetagger/cmd
